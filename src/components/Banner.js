import React, { Component } from 'react';
import '../Banner.css';
class Banner extends Component {
    constructor(props) {
        super(props)
        this.state = {
            areaName: 'Select City',
            specialistName: 'Select Speciality'
        }

        this.handleAreaChange = this.handleAreaChange.bind(this);
        this.handleSpecialistChange = this.handleSpecialistChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        // alert('A form was submitted: ' + this.state.areaName + "   " + this.state.diseaseName);
        event.preventDefault();
        var v1 = this.state.areaName;
        var v2 = this.state.specialistName;
        if (v1 === 'Select City' || v2 === 'Select Speciality') {
            document.getElementById('requiredField').style.background = "rgba(195,194,194, 0.2)";
            document.getElementById('fieldHeading').innerHTML = "These Field are required !"
        }
        else {
            document.getElementById('requiredField').style.background = "none";
            document.getElementById('fieldHeading').innerHTML = "";
        }
        this.setState({
            areaName: 'Select City',
            specialistName: 'Select Speciality'
        })
    }

    handleAreaChange(event) {

        var v = event.target.value;
        this.setState({
            areaName: v
        })
    }

    handleSpecialistChange(event) {
        var v = event.target.value;
        this.setState({
            specialistName: v
        })
    }





    render() {
        return (


            <div className="banner">
                <div className="bg-color">

                    <div className="form-container">

                        <div className="caption">
                            <h3>Find Your Nearest Doctor</h3>
                        </div>

                        <form className="searchDoc" onSubmit={this.handleSubmit}>
                            <div className="row">
                                <div className="col-md-4 pr-1">
                                        <select className="browser-default" value={this.state.areaName} onChange={this.handleAreaChange}>
                                            <option value="Select City">Select City</option>
                                            <option value="lahore">Lahore</option>
                                            <option value="karachi">Karachi</option>
                                            <option value="multan">Multan</option>
                                            <option value="sialkot">Sialkot</option>
                                        </select>
                                </div>

                                <div className="col-md-4 pr-1">
                                        <select className="browser-default" value={this.state.specialistName} onChange={this.handleSpecialistChange}>
                                            <option value="Select Speciality">Select Speciality</option>
                                            <option value="as">Allergy Specialist</option>
                                            <option value="bs">Breast Surgeon</option>
                                            <option value="cs">Cancer Specialist</option>
                                            <option value="d">Dentist</option>
                                            <option value="ent">E.N.T Specialist</option>
                                            <option value="fsc">Fitness & Slimming Consultant</option>
                                        </select>
                                </div>

                                <div className="col-md-4">
                                    <button type="submit" className="submitButton">
                                        <span>find doctors</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div id="requiredField">
                            <p id="fieldHeading"></p>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}
export default Banner;