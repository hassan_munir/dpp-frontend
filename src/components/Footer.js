import React, { Component } from 'react';
import '../Footer.css';
class Footer extends Component {

  render() {
    return (
      <div id="footer" className="footer">

        <div className="footerBox">
          <div className="footerBox1">
            <div className="box">
              <h3>MedCure</h3>
              <p>"We aim to bring a change
              in the society<br></br>  and make people
              aware of the diseases<br></br> and their cures"</p>
            </div>
          </div>
          <div className="footerBox2">
            <div className="box">
              <h3>Keep Connected</h3>
              <ul className="boxlinks">
                <li className="phone"><i class="tiny fa fa-phone icon-blue"></i> +92 305 9328783</li>
                <li><i class="tiny fa fa-facebook icon-blue"></i><a href="https://web.facebook.com"> facebook</a></li>
                <li><i class="tiny material-icons icon-blue">mail</i><a href="https://accounts.google.com/ServiceLogin/signinchooser"> email</a></li>
              </ul>
            </div>
          </div>
          <div className="footerBox3">
            <div className="box">
              <h3>Useful links</h3>
              <ul className="boxlinks">
                <li><a href="https://www.cdc.gov/diseasesconditions/az/a.html">diseases</a></li>
                <li><a href="http://www.gov.pe.ca/photos/original/WI_KRemedies.pdf">home remedies</a></li>
                <li><a href="https://www.cancer.gov/about-cancer/understanding/what-is-cancer">cancer info</a></li>
              </ul>
            </div>
          </div>
        </div>
        
        <hr></hr>
        <div className="credits">
          ©2019 MedCure. All rights reserved<br></br>
          Designed by <a href="https://www.fiverr.com/technocares">technoCares</a>
        </div>
      
      </div>
    );
  }
}
export default Footer;