import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import About from './About';
import Navbar from './Navbar';
import Banner from './Banner';
import Login from './Login';
import Doctors from './Doctors';
import Home from './Home';

const Routes = () => (
    <Router>
        <div>
            <Navbar />
            <Banner/>
            <Route exact path='/' component={Home} />
            <Route exact path='/diseases' component={Home} />
            <Route exact path='/doctors' component={Doctors} />
            <Route exact path='/signin' component={Login} />
            <Route exact path='/about' component={About} />
        </div>
    </Router>
)
export default Routes;