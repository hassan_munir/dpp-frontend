import React from 'react';
import '../About.css';
const About = () => {
    return(
        <div className="container">
            <h4 className="center">About</h4>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Animi commodi quasi itaque, expedita id voluptas veniam soluta numquam a nostrum magni sed doloremque amet quidem velit recusandae? Veniam, maiores dolor?</p>
        </div>
    );
}
export default About;