import React from 'react';
import '../Diseases.css';
import A from '../Images/a.jpg';
import B from '../Images/b.jpg';
import C from '../Images/c.jpg';
import D from '../Images/d.jpg';
import E from '../Images/e.jpg';
import F from '../Images/f.jpg';
const Diseases = () => {
    return (
        <section id="diseases" className="section-padding">
            <div className="container">
                <div className="row">
                    <div className="header-section text-center col-md-12 col-sm-12">
                        <h2>Diseases</h2>
                        <p>We aim to bring a change in the society and make people<br></br> aware of the diseases and their cures.</p>
                        <hr className="diseases-line"></hr>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-md-4 col-sm-6 padleft-right">
                        <figure className="imghvr-fold-up">
                            <img src={A} className="img-responsive"></img>
                            <figcaption>
                                <h3>Acinetobacter Infection</h3>
                                <p>Acinetobacter baumannii is a typically short, almost round, rod-shaped Gram-negative bacterium. It can be an opportunistic pathogen in humans.</p>
                            </figcaption>
                            <a href="https://en.wikipedia.org/wiki/Acinetobacter_baumannii"></a>
                        </figure>
                    </div>
                    <div className="col-md-4 col-sm-6 padleft-right">
                        <figure className="imghvr-fold-up">
                            <img src={B} className="img-responsive" />
                            <figcaption>
                                <h3>Birth Defects</h3>
                                <p>If a baby is born with a part of the body that is missing or malformed, it is called a structural birth defect. Heart defects are the most common type of structural defect.</p>
                            </figcaption>
                            <a href="https://en.wikipedia.org/wiki/Birth_defect"></a>
                        </figure>
                    </div>
                    <div className="col-md-4 col-sm-6 padleft-right">
                        <figure className="imghvr-fold-up">
                            <img src={C} className="img-responsive"></img>
                            <figcaption>
                                <h3>Canine Flu</h3>
                                <p>Canine influenza (dog flu) is influenza occurring in canine animals. Canine influenza is caused by varieties of influenzavirus A, such as equine influenza virus H3N8.</p>
                            </figcaption>
                            <a href="https://en.wikipedia.org/wiki/Canine_influenza"></a>
                        </figure>
                    </div>


                    <div className="col-md-4 col-sm-6 padleft-right">
                        <figure className="imghvr-fold-up">
                            <img src={D} className="img-responsive"></img>
                            <figcaption>
                                <h3>Diabetes</h3>
                                <p>Diabetes is a disease in which your blood glucose, or blood sugar, levels are too high. Glucose comes from the foods you eat.</p>
                            </figcaption>
                            <a href="https://en.wikipedia.org/wiki/Diabetes_mellitus"></a>
                        </figure>
                    </div>
                    <div className="col-md-4 col-sm-6 padleft-right">
                        <figure className="imghvr-fold-up">
                            <img src={E} className="img-responsive"></img>
                            <figcaption>
                                <h3>Extreme Heat [Hyperthermia]</h3>
                                <p>Hyperthermia. Hyperthermia is elevated body temperature that occurs when a body produces or absorbs more heat than it dissipates.</p>
                            </figcaption>
                            <a href="https://en.wikipedia.org/wiki/Hyperthermia"></a>
                        </figure>
                    </div>
                    <div className="col-md-4 col-sm-6 padleft-right">
                        <figure className="imghvr-fold-up">
                            <img src={F} className="img-responsive"></img>
                            <figcaption>
                                <h3>Fungal Eye Infections</h3>
                                <p>Fungal eye infections are extremely rare, but they can be very serious. ... Inflammation or infection of the cornea (the clear, front layer of the eye).</p>
                            </figcaption>
                            <a href="https://en.wikipedia.org/wiki/Fungal_keratitis"></a>
                        </figure>
                    </div>
                </div>
            </div>
        </section>
    );
}
export default Diseases;