import React from 'react';
import '../Navbar.css';
import {Link, withRouter} from 'react-router-dom';

const Navbar=(props)=>{
    return(
        <div className="navbar-fixed">
        <nav className="nav-wrapper">
            <div className="container">
            <a className="brand-logo">MedCure</a>
                <ul className="list">
                    <li><Link to='/' >Home</Link></li>
                    <li><Link to='/diseases' >Diseases</Link></li>
                    <li><Link to='/doctors' >Doctors</Link></li>
                    <li><Link to='/signin' >Sign in</Link></li>
                    <li><Link to='/about' >About us</Link></li>
                </ul>
            </div>
        </nav>
        </div>
    )
}
export default withRouter(Navbar);