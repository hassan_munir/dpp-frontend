import React from 'react';
import '../Customer.css';

const Customer = () => {
    return (
        <section id="testimonial" className="section-padding">
            <div className="container">
                <div className="row">
                    <div className="header-section text-center col-md-12 col-sm-12">
                        <h2 className="comment-heading">See What Our Customer Are Saying?</h2>
                        <p className="comment-heading">We aim to bring a change
              in the society and make people<br></br> aware of the diseases and their cures.</p>
                        <hr className="comment-line"></hr>
                    </div>
                    <div className="col-md-6 col-sm-6">
                        <div className="comment-box">
                            <p className="text-par">"Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, nec sagittis sem"</p>
                            <p className="text-name">Abraham Doe - Creative Dırector</p>
                        </div>
                    </div>
                    <div className="col-md-6 col-sm-6">
                        <div className="comment-box">
                            <p className="text-par">"Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, nec sagittis sem"</p>
                            <p className="text-name">Abraham Doe - Creative Dırector</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
export default Customer;