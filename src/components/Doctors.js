import React, { Component } from 'react';
import { connect } from 'react-redux';


class Doctors extends Component {
    render() {
        console.log(this.props);
        const { doctors } = this.props;
        const doctorList = doctors.length ? (
            doctors.map(doctor => {
                return (
                    <div className="post card" key={doctor.id}>
                        <div className="card-content">
                            <span className="card-title">{doctor.title}</span>
                            <p>{doctor.hospital}</p>
                        </div>       
                    </div>
                )
            })
        ) : (
                <div className="conatiner">
                    <h5>No Doctors Found!!!</h5>
                </div>
            )

        return (
            <div className="container">
                <h4 className="center">Doctors</h4>
                {doctorList}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        doctors: state.doctors
    }
}

const mapDispatchToProps=()=>{
    
}

export default connect(mapStateToProps)(Doctors);