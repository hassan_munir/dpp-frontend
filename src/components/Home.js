import React from 'react';

const Home=()=>{
    return (
        <div id="outer" className="outer">

            <div className="tab-container">

                <div className="tab-container-Box1">
                    <div className="box">
                        <h3>Dr. Umair Ahmed Khan</h3>
                        <p>(Consultant Family Physician<br></br>
                            and Male Fertility Health)</p>
                        <h6>Sialkot Medical Complex</h6>
                    </div>
                </div>

                <div className="tab-container-Box2">
                    <div className="box">
                        <h3>Timmings</h3>
                        <p>Monday to Saturday (Morning)<br></br>
                            (10:00AM-4:00PM)</p>
                    </div>
                </div>

                <div className="tab-container-Box3">
                    <div className="box">
                        <h3>Contact Details</h3>
                        <p><i class="tiny fa fa-phone"></i><span className="phone-number"> +92 0305 9328783</span><br></br>
                            <i class="tiny material-icons">place</i><span className="address"> Adam Road near Civil Hospital, Sialkot</span>
                        </p>
                    </div>
                </div>

                <div className="tab-container-Box3">
                    <div className="box">
                        <h3>Make an Appointment</h3>
                        <div className="btn-group">
                            <button>Book Appointment</button>
                            <button>Cancel Appointment</button>
                            <button>See Appointment</button>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    );
}
export default Home;