import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Footer from './components/Footer';
import Routes from './components/Routes';
import Customer from './components/Customer';
import Diseases from './components/Diseases';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";


class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Routes />
          <Customer/>
          <Diseases/>
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
